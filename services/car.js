const CarModel = require("../models/car")

const carModel = new CarModel()

class CarServices {

    async getAllCars() {
        return await carModel.getAllCars()
    }

    async addCar(car, userId) {
        const carId = await carModel.addCar(car, userId)
        const features = this.createCarFeaturesArray(car.features, carId)
        await carModel.addCarFeature(features)
        await carModel.addCarAvailability(carId, car.availablity)
        return { status: 200, message: "Car has been listed.", car_id: carId }
    }

    async updateCar(car, userId) {
        await carModel.deleteAllCarFeatures(car.car_id)
        const features = this.createCarFeaturesArray(car.features, car.car_id)
        await carModel.addCarFeature(features)
        return await carModel.updateCar(car, userId)
    }

    async updateCarAvailability(availablity, carId) {
        return await carModel.updateCarAvailability(carId, availablity)
    }

    async addCarImages(carId, images) {
        const imgs = this.createCarImagesArray(images, carId)
        return await carModel.addCarImages(imgs)
    }

    async updateCarImages(carId, images) {
        await carModel.deleteAllCarImages(carId)
        return await this.addCarImages(carId, images)
    }

    async setAvailabilityStatus(status, carId) {
        return await carModel.setAvailabilityStatus(status, carId)
    }

    async setCarFuelAvailabilityStatus(status, carId) {
        return await carModel.setCarFuelAvailabilityStatus(status, carId)
    }

    async setCarCaptainAvailabilityStatus(status, carId) {
        return await carModel.setCarDropAndPickStatus(status, carId)
    }

    async setCarDriverAvailabilityStatus(status, carId) {
        return await carModel.setCarDriverAvailabilityStatus(status, carId)
    }

    async getAllFeatures() {
        return await carModel.getAllFeatures()
    }

    async createFeature(name) {
        return await carModel.createFeature(name)
    }

    async updateFeature(id, name) {
        return await carModel.updateFeature(id, name)
    }


    createCarImagesArray(images, carId) {
        const carImages = []
        images.forEach(element => {
            carImages.push({ image_url: element, car_id: carId })
        });
        return carImages
    }

    createCarFeaturesArray(features, carId) {
        const feature = []
        features.forEach(element => {
            feature.push({ feature_id: element, car_id: carId })
        });
        return feature
    }
}

module.exports = CarServices