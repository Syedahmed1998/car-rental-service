const BookingModel = require("../models/booking")

const bookingModel = new BookingModel()

class BookingServices {

    async createBooings(booking, user_id) {
        await bookingModel.createBooings(booking, user_id)
    }
    async getAllCustomerBooking(user_id) {
        await bookingModel.getAllCustomerBooking(user_id)
    }
    async getVendorBookings(user_id) {
        await bookingModel.getVendorBookings(user_id)
    }
    async updateBookingStatus(booking_id, status) {
        await bookingModel.updateBookingStatus(booking_id, status)
    }

}

module.exports = BookingModel