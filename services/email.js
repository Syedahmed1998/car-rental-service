"use strict";
const nodeMailer = require("nodemailer");
const dotenv = require("dotenv");
dotenv.config();

class NodeEmailer{
    SendEmail(email,subject, body) {
        this.transporter.sendMail({
            from: process.env.EMAIL, // sender address
            to: email, // list of receivers
            subject: subject, // Subject line
            text: body // plain text body
          });
    }

    transporter = nodeMailer.createTransport({
        service: process.env.EMAIL_SERVICE,
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASSWORD
        }
      });
}



module.exports = NodeEmailer
