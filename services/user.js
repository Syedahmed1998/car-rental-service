const UserModel = require("../models/User")
const NodeEmailer = require("./email")
const jwt = require("jsonwebtoken")
const dotenv = require("dotenv")

dotenv.config()
const userModel = new UserModel()
const nodeEmailer = new NodeEmailer()

class UserServices {


    async isUserExist(phoneNumber) {
        const doesExist = await userModel.isUserExist(phoneNumber)
        if (doesExist) return await userModel.getProfileStatus(doesExist.user_id)
        else {
            const userId = await userModel.createUser(phoneNumber)
            return await userModel.craeteProfileStatus(userId)
        }
    }

    async requireOTP(phoneNumber) {
        return await userModel.requestOTP(phoneNumber)
    }

    async verifyNumber(phoneNumber, code) {
        const result = await userModel.verifyPhoneVerificationCode(phoneNumber, code)
        return await userModel.updateUserPhoneVerification(result.user_id)
        
    }

    async setPassword(id, password) {
        await userModel.getUserById(id)
        await userModel.setPassword(id, password)
        await userModel.updateUserSetPassword(id)
        const data = await userModel.getUserData(id)
        const token = jwt.sign({data}, process.env.ACCESS_TOKEN_SECRET)
        return { status: 200, token: token }
    }

    async updateProfile(user) {
        await userModel.getUserById(user.user_id)
        await userModel.updateProfile(user)
        await userModel.updateUserProfileCreated(user.user_id)
        const data = await userModel.getUserData(user.user_id)
        const token = jwt.sign({ data }, process.env.ACCESS_TOKEN_SECRET)
        data.token = token
        data.verification_code = null
        return { status: 200, user: data }
    }

    async signIn(phone_number, password) {
        const data = await userModel.signIn(phone_number, password)
        const token = jwt.sign({ data }, process.env.ACCESS_TOKEN_SECRET)
        data.token = token
        data.verification_code = null
        return { status: 200, user: data }

    }

    async uploadLicense(id,license_front,license_back) {        
        await userModel.getUserById(id)
        await userModel.updateLicense(id,license_front,license_back)
        const result = await userModel.updateUserLicenseUploaded(id)
        return result
    }

    async uploadCnic(id,id_front,id_back) {        
        await userModel.getUserById(id)
        await userModel.uploadCnic(id,id_front,id_back)
        const result = await userModel.updateUserCnicUploaded(id)
        return result
    }

    async updateCnic(id,id_front,id_back) {     
        await userModel.getUserById(id)   
        const data =  await userModel.updateCnic(id,id_front,id_back)
        return data
    }

    async getDocuments(id) {
        const data =  await userModel.getDocuments(id)
        return data
    }

    async getUnverifiedDocuments() {
        return await userModel.getVerifiedAndUnverifiedDocuments(0)
    }

    async verifyDocuments(user_id) {
        return await userModel.updateUserDocumentVerification(user_id)
    }

    async getVerifiedDocuments() {
        return await userModel.getVerifiedAndUnverifiedDocuments(1)
    }

    async getDocumentsOfAllUsers() {
        return await userModel.getDocumentsOfAllUsers()

    }

    async writeReviewOnDocument(documentId, review) {
        return await userModel.writeReviewOnDocument(documentId, review)
    }
    
    async getAllUsers(account_type) {
        return await userModel.getAllUsers(account_type)
    }


    async updatePassword(id, password) {
        const result = await userModel.getUserById(id)
        if (result.length) {
            const data = await userModel.updatePassword(id, password)
            return data
        } else return result
    }




}

module.exports = UserServices