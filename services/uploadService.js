const express = require("express");
const multer = require("multer");
const path = require("path");
const AWS = require("aws-sdk");
const authoriser = require('../utility/autheriser')
const dotenv = require("dotenv");
const { v4: uuidv4 } = require("uuid");
const Validations = require("../validators/validators");

dotenv.config();
const router = express.Router();
router.use(express.json());
const validations = new Validations()



s3BucketStorage = new AWS.S3({
  accessKeyId: process.env.AWS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET
});

storage = multer.memoryStorage({
  destination: function (req, file, callBack) {
    cb(null, "/images");
  }
});

upload = multer({
  storage: this.storage
}).single("file");


router.post(
  "/uploadFile",
  authoriser.authenticateToken, 
  async (req, res) => {
    try {
      upload(req, res, err => {
        if (!req.file) res.status(409).json({ status: 409, message: `file is required.` });

        let fileInfo = req.file;
        let fileName = fileInfo.originalname.split(".");
        let fileType = fileName[fileName.length - 1];
        
        if (!fileType.match(/jpg|JPG|jpeg|JPEG|png|PNG/g)) {
          res.status(409).json({ status: 409, message: `please provide image` });
        } else if (err instanceof multer.MulterError) {
          res.status(409).json({ status: 409, message: err.message });
        } else if (err) {
          res.status(409).json({ status: 409, message: err.message });
        }

        const params = {
          Bucket: process.env.AWS_BUCKET_NAME,
          Key: `${uuidv4()}.${fileType}`,
          Body: fileInfo.buffer
        };

        s3BucketStorage.upload(params, (error, data) => {
          if (error) res.status(500).json({ status: 500, message: error.message });
          else res.status(200).json({ status: 200, url: data.Location });
        });
      });

    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

module.exports = router