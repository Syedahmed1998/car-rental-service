const Schema = require('./schemas')

const schemas = new Schema()

class Validations {

    validatePhoneNumber = function (req, res, next) {
        const { error } = schemas.phoneNumber().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateAccountType = function (req, res, next) {
        const { error } = schemas.accountType().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateUserId = function (req, res, next) {
        const { error } = schemas.userId().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateReview = function (req, res, next) {
        const { error } = schemas.review().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validatePassword = function (req, res, next) {
        const { error } = schemas.password().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateSignIn = function (req, res, next) {
        const { error } = schemas.signIn().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateCode = function (req, res, next) {
        const { error } = schemas.codeVeriification().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    cnic = function (req, res, next) {
        console.log(req.file)
        const { error } = schemas.cnic().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    license = function (req, res, next) {
        console.log(req.file)
        const { error } = schemas.license().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateCode = function (req, res, next) {
        const { error } = schemas.codeVeriification().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateUser = function (req, res, next) {
        const { error } = schemas.user().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }


    //admin
    validateAdmin = function (req, res, next) {
        const { error } = schemas.adminRight().validate(req.user.data)
        if (error) return res.status(403).json({ status: 403, message: error.message })
        else next()
    }

    validateRenter = function (req, res, next) {
        const { error } = schemas.renterRight().validate(req.user.data)
        if (error) return res.status(403).json({ status: 403, message: error.message })
        else next()
    }

    validateTenant = function (req, res, next) {
        const { error } = schemas.tenantRight().validate(req.user.data)
        if (error) return res.status(403).json({ status: 403, message: error.message })
        else next()
    }

    validateFeatureName = function (req, res, next) {
        const { error } = schemas.featureName().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateFeature = function (req, res, next) {
        const { error } = schemas.feature().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    //car

    validateCreateCar = function (req, res, next) {
        const { error } = schemas.createCar().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateUpdateCar = function (req, res, next) {
        const { error } = schemas.updateCar().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateCarImage = function (req, res, next) {
        const { error } = schemas.setUpdateCarImages().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateSetCarAvailability = function (req, res, next) {
        const { error } = schemas.setCarAvailability().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateSetCarDriverAvailability = function (req, res, next) {
        const { error } = schemas.setCarDriverAvailability().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateSetCarFuelAvailability = function (req, res, next) {
        const { error } = schemas.setCarFuelAvailability().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateSetCarCaptainAvailabilityStatus = function (req, res, next) {
        const { error } = schemas.setCarDropAndPickStatus().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    validateUpdateCarAvailability = function (req, res, next) {
        const { error } = schemas.updateCarAvailability().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

    //booking
    validateBooking = function (req, res, next) {
        const { error } = schemas.createBooking().validate(req.body)
        if (error) return res.status(409).json({ status: 409, message: error.message })
        else next()
    }

}

module.exports = Validations