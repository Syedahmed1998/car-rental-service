const Joi = require("joi");

class Schema {

    phoneNumber() {
        return Joi.object().keys({
            phoneNumber: Joi.string().max(13).min(13).required()
        })
    }

    accountType() {
        return Joi.object().keys({
            account_type: Joi.string().valid("renter", "tenant", "both", "admin").required()
        })
    }

    review() {
        return Joi.object().keys({
            review: Joi.string().min(1).required(),
            document_id: Joi.string().required()
        })
    }

    codeVeriification() {
        return Joi.object().keys({
            phoneNumber: Joi.string().max(13).min(13).required(),
            code: Joi.string().max(6).min(6).required()
        })
    }

    password() {
        return Joi.object().keys({
            user_id: Joi.number().required(),
            password: Joi.string().min(6).required()
        })
    }

    userId() {
        return Joi.object().keys({
            user_id: Joi.number().required()
        })
    }

    signIn() {
        return Joi.object().keys({
            phone_number: Joi.number().required(),
            password: Joi.string().min(6).required()
        })
    }

    cnic() {
        return Joi.object().keys({
            user_id: Joi.number().required(),
            id_front: Joi.string().required(),
            id_back: Joi.string().required()
        })
    }

    license() {
        return Joi.object().keys({
            user_id: Joi.number().required(),
            license_front: Joi.string().required(),
            license_back: Joi.string().required()
        })
    }

    user() {
        return Joi.object().keys({
            user_id: Joi.number().required(),
            name: Joi.string().required(),
            email: Joi.string().email(),
            id_card_number: Joi.string().required(),
            city: Joi.string().required(),
            state: Joi.string().required(),
            country: Joi.string().required(),
            gender: Joi.string().valid("male", "female").required(),
            account_type: Joi.string().valid("renter", "tenant", "both", "admin").required()
        })
    }

    //admin
    adminRight() {
        return Joi.object().keys({
            account_type: Joi.string().valid("admin").required().error(Error("Only admin has rights to access this resource."))
        }).unknown()
    }

    renterRight() {
        return Joi.object().keys({
            account_type: Joi.string().valid("renter", "both").required().error(Error("Only renter has rights to access this resource."))
        }).unknown()
    }

    tenantRight() {
        return Joi.object().keys({
            account_type: Joi.string().valid("tenant", "both").required().error(Error("Only tenant has rights to access this resource."))
        }).unknown()
    }

    feature() {
        return Joi.object().keys({
            feature_name: Joi.string().required(),
            feature_id: Joi.string().required(),
        }).unknown()
    }

    featureName() {
        return Joi.object().keys({
            feature_name: Joi.string().required()
        }).unknown()
    }

    //car

    createCar() {
        return Joi.object().keys({
            model: Joi.string().required(),
            company: Joi.string().required(),
            made_year: Joi.number().required(),
            is_insured: Joi.boolean().required(),
            transmission: Joi.string().valid("auto", "manual").required(),
            color: Joi.string().required(),
            type: Joi.string().valid("pickup truck", "minivan", "suv", "convertible", "hatchback", "station wagon", "coupe", "sadan", "sports car").required(),
            registration_number: Joi.string().required(),
            features: Joi.array().items(Joi.string()).min(1).required(),
            availablity: Joi.object().keys({
                time_from: Joi.string().required(),
                time_to: Joi.string().required(),
                min_hours: Joi.string().required(),
                max_hours: Joi.string().required(),
                days: Joi.array().items(Joi.boolean()).min(7).max(7).required(),
                is_fuel_included: Joi.boolean().required(),
                is_driver_available: Joi.boolean().required(),
                is_captain_available: Joi.boolean().required()
            }).required(),
            price: Joi.string().required()
        })
    }

    updateCar() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            model: Joi.string().required(),
            company: Joi.string().required(),
            made_year: Joi.number().required(),
            is_insured: Joi.boolean().required(),
            transmission: Joi.string().valid("auto", "manual").required(),
            color: Joi.string().required(),
            type: Joi.string().valid("pickup truck", "minivan", "suv", "convertible", "hatchback", "station wagon", "coupe", "sadan", "sports car").required(),
            registration_number: Joi.string().required(),
            features: Joi.array().items(Joi.string()).min(1).required(),
            price: Joi.string().required()
        })
    }

    updateCarAvailability() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            time_from: Joi.string().required(),
            time_to: Joi.string().required(),
            min_hours: Joi.string().required(),
            max_hours: Joi.string().required(),
            days: Joi.array().items(Joi.boolean()).min(7).max(7).required(),
            is_available: Joi.boolean().required(),
            is_fuel_included: Joi.boolean().required(),
            is_driver_available: Joi.boolean().required(),
            is_captain_available: Joi.boolean().required()
        })
    }

    setCarAvailability() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            is_available: Joi.boolean().required()
        })
    }

    setCarDriverAvailability() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            is_driver_available: Joi.boolean().required()
        })
    }

    setCarFuelAvailability() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            is_fuel_included: Joi.boolean().required()
        })
    }

    setCarCaptainAvailabilityStatus() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            is_captain_available: Joi.boolean().required()
        })
    }

    setUpdateCarImages() {
        return Joi.object().keys({
            car_id: Joi.string().required(),
            images: Joi.array().items(Joi.string()).min(1).required()
        })
    }

    //booking
    createBooking() {
        return Joi.object().keys({
            vendor_id: Joi.string().required(),
            user_id: Joi.string().required(),
            car_id: Joi.string().required(),
            status: Joi.boolean().required(),
            date: Joi.string().required(),
            pick_up_receiving_time: Joi.string().required(),
            total_time: Joi.string().required(),
        })
    }

}

module.exports = Schema