const express = require("express");
const Validations = require("./validators/validators");
const user = require('../car_rental_application/routes/user')
const car = require('../car_rental_application/routes/car')
const booking = require('../car_rental_application/routes/booking')
const message = require("../car_rental_application/helper/sendMesseges")
const uploadService = require("../car_rental_application/services/uploadService");

const validations = new Validations()
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//routes
app.use('/api/auth',user)
app.use('/api/car',car)
app.use('/api/booking',booking)
app.use('/api/upload',uploadService)
app.use('/api/messages',message)

// app.get("/exp",validations.validateName, (req, res) => {
//     res.send("Document for car rental");
//   });


  const PORT = process.env.PORT || 3000;

  app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});