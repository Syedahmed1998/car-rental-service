const express = require('express');
const authoriser = require('../utility/autheriser')
const BookingServices = require('../services/booking');
const Validations = require("../validators/validators");

// initializations
const validations = new Validations()
const bookingServices = new BookingServices()
const router = express()

router.post("/createBooking", authoriser.authenticateToken, validations.validateTenant,validations.validateBooking, async (req, res) => {
    try {
        const result = await bookingServices.createBooings(req.body, req.user.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.get("/getCustomerBookings", authoriser.authenticateToken, validations.validateTenant, async (req, res) => {
    try {
        const result = await bookingServices.getAllCustomerBooking(req.user.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.get("/getVendorBookings", authoriser.authenticateToken, validations.validateRenter, async (req, res) => {
    try {
        const result = await bookingServices.getVendorBookings(req.user.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.get("/updateBookingStatus", authoriser.authenticateToken, validations.validateRenter, async (req, res) => {
    try {
        const result = await bookingServices.updateBookingStatus(req.body.booking_id, req.body.status)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

module.exports = router