const express = require('express');
const authoriser = require('../utility/autheriser')
const CarServices = require('../services/car');
const Validations = require("../validators/validators");

// initializations
const validations = new Validations()
const carServices = new CarServices()
const router = express()

router.get("/getAllCars", async (req, res) => {
    try {
        const result = await carServices.getAllCars()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.post('/addCar', authoriser.authenticateToken, validations.validateRenter, validations.validateCreateCar, async (req, res) => {
    try {
        const result = await carServices.addCar(req.body, req.user.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/updateCar', authoriser.authenticateToken, validations.validateRenter, validations.validateUpdateCar, async (req, res) => {
    try {
        const result = await carServices.updateCar(req.body, req.user.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/updateCarAvailablity', authoriser.authenticateToken, validations.validateRenter, validations.validateUpdateCarAvailability, async (req, res) => {
    try {
        const result = await carServices.updateCarAvailability(req.body, req.body.car_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.post('/addCarImages', authoriser.authenticateToken, validations.validateRenter, validations.validateCarImage, async (req, res) => {
    try {
        const result = await carServices.addCarImages(req.body.car_id, req.body.images)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/updateCarImages', authoriser.authenticateToken, validations.validateRenter, validations.validateCarImage, async (req, res) => {
    try {
        const result = await carServices.updateCarImages(req.body.car_id, req.body.images)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/setAvailabilityStatus', authoriser.authenticateToken, validations.validatesetCarAvailability, async (req, res) => {
    try {
        const result = await carServices.setAvailabilityStatus(req.body.is_available, req.body.car_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/setCarDriverAvailabilityStatus', authoriser.authenticateToken, validations.validatesetCarDriverAvailability, async (req, res) => {
    try {
        const result = await carServices.setCarDriverAvailabilityStatus(req.body.is_driver_available, req.body.car_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/setCarFuelAvailabilityStatus', authoriser.authenticateToken, validations.validatesetCarFuelAvailability, async (req, res) => {
    try {
        const result = await carServices.setCarFuelAvailabilityStatus(req.body.is_fuel_included, req.body.car_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/setCarCaptainAvailabilityStatus', authoriser.authenticateToken, validations.validatesetCarCaptainAvailabilityStatus, async (req, res) => {
    try {
        const result = await carServices.setCarCaptainAvailabilityStatus(req.body.have_to_pick_the_car, req.body.car_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.get('/getAllFeatures', async (req, res) => {
    try {
        const result = await carServices.getAllFeatures()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.post('/createFeature', authoriser.authenticateToken, validations.validateAdmin, validations.validateFeatureName, async (req, res) => {
    try {
        const result = await carServices.createFeature(req.body.feature_name)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.put('/updateFeature', authoriser.authenticateToken, validations.validateAdmin, validations.validateFeature, async (req, res) => {
    try {
        const result = await carServices.updateFeature(req.body.feature_id, req.body.feature_name)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).json(error)
    }
})

module.exports = router