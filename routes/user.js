const express = require('express');
const authoriser = require('../utility/autheriser')
const UserServices = require('../services/user');
const Validations = require("../validators/validators");

// initializations
const validations = new Validations()
const userServices = new UserServices()
const router = express()


router.post('/isUserExist', validations.validatePhoneNumber, async (req, res) => {
    try {
        const result = await userServices.isUserExist(req.body.phoneNumber)
        await userServices.requireOTP()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(500).send(error.message)
    }
})

router.post('/verifyNumber', validations.validateCode, async (req, res) => {
    try {
        const result = await userServices.verifyNumber(req.body.phoneNumber, req.body.code)
        res.status(result.status).json({ status: result.status, message: "Phone number has been verified." })
    } catch (error) {
        res.status(error.status).json(error)
    }
})

router.post('/setPassword', validations.validatePassword, async (req, res) => {
    try {
        const result = await userServices.setPassword(req.body.user_id, req.body.password)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.post('/requestOTP', validations.validatePhoneNumber, async (req, res) => {
    try {
        const result = await userServices.requireOTP(req.body.phoneNumber)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.put('/updatePassword', authoriser.authenticateToken, validations.validatePassword, async (req, res) => {
    try {
        const result = await userServices.updatePassword(req.body.user_id, req.body.password)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.put('/updateProfile', authoriser.authenticateToken, validations.validateUser, async (req, res) => {
    try {
        const result = await userServices.updateProfile(req.body)
        res.status(result.status).json(result)
    } catch (error) {
        res.send(error)
    }
})

router.post("/signIn", validations.validateSignIn, async (req, res) => {
    try {
        const result = await userServices.signIn(req.body.phone_number, req.body.password)
        res.json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})


router.post('/uploadCnic', authoriser.authenticateToken, validations.cnic, async (req, res) => {
    try {
        const result = await userServices.uploadCnic(req.body.user_id, req.body.id_front,
            req.body.id_back)
        res.json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.put('/updateCnic', authoriser.authenticateToken, validations.cnic, async (req, res) => {
    try {
        const result = await userServices.updateCnic(req.body.user_id, req.body.id_front,
            req.body.id_back)
        res.json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.put('/updateLicense', authoriser.authenticateToken, validations.license, async (req, res) => {
    try {
        const result = await userServices.uploadLicense(req.body.user_id, req.body.license_front, req.body.license_back)
        res.json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.get('/getDocuments', authoriser.authenticateToken, validations.validateUserId, async (req, res) => {
    try {
        const result = await userServices.getDocuments(req.body.user_id)
        res.json(result)
    } catch (error) {
        res.send(error)
    }
})

// API for admin role

router.post("/verifyDocuments", authoriser.authenticateToken, validations.validateAdmin, validations.validateUserId, async (req, res) => {
    try {
        const result = await userServices.verifyDocuments(req.body.user_id)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.get("/getUnverifiedDocuments", authoriser.authenticateToken, validations.validateAdmin, async (req, res) => {
    try {
        const result = await userServices.getUnverifiedDocuments()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.get("/getVerifiedDocuments", authoriser.authenticateToken, validations.validateAdmin, async (req, res) => {
    try {
        const result = await userServices.getVerifiedDocuments()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.get("/getDocumentsOfAllUsers", authoriser.authenticateToken, validations.validateAdmin, async (req, res) => {
    try {
        const result = await userServices.getDocumentsOfAllUsers()
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.post("/writeReviewOnDocument", authoriser.authenticateToken, validations.validateAdmin, validations.validateReview, async (req, res) => {
    try {
        const result = await userServices.writeReviewOnDocument(req.body.document_id, req.body.review)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

router.get("/getAllUsers", authoriser.authenticateToken, validations.validateAdmin, validations.validateAccountType, async (req, res) => {
    try {
        const result = await userServices.getAllUsers(req.body.account_type)
        res.status(result.status).json(result)
    } catch (error) {
        res.status(error.status).send(error)
    }
})

module.exports = router