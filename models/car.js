const dbConnection = require("../models/db_config");

class CarModel {

    async getAllCars() {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select id,model,company,made_year,transmission,color,type,registration_number,is_insured,price,
            Car_rental.car_availability.time_from,Car_rental.car_availability.time_to,
            Car_rental.car_availability.min_hours,Car_rental.car_availability.max_hours,Car_rental.car_availability.monday,
            Car_rental.car_availability.tuesday,Car_rental.car_availability.wednesday,Car_rental.car_availability.thursday,
            Car_rental.car_availability.friday,Car_rental.car_availability.saturday,Car_rental.car_availability.sunday,Car_rental.car_availability.is_available,
            group_concat(Car_rental.car_images.image_url) as images
            from car
                join car_availability on availability_id = car.id
                join car_images on car_images.car_id = Car_rental.car.id group by car.id`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No car found." })
                else {
                    res.map(item => { item.images = item.images.split(",") })
                    resolve({ status: 200, cars: res });
                }
            });
        });
    }

    async addCar(car, userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into car(
                user_id,model, company, made_year, transmission, color, type, registration_number,price, is_insured
            ) values('${userId}','${car.model}','${car.company}','${car.made_year}','${car.transmission}','${car.color}'
            ,'${car.type}','${car.registration_number}', '${car.price}','${car.is_insured}')`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve(res.insertId);
            });
        });
    }

    async updateCar(car, userId) {
        console.log(car)
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car set model = '${car.model}', company = '${car.company}', made_year = '${car.made_year}'
            , transmission = '${car.transmission}', color = '${car.color}', type = '${car.type}', registration_number = '${car.registration_number}'
            , is_insured = '${car.is_insured}', price='${car.price}' where id = '${car.car_id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                else resolve({ status: 200, message: "Car has been updated." });
            });
        });
    }

    async addCarImages(images) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into car_images(image_url,car_id) values ?`, [images.map(item => [item.image_url, item.car_id])],
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else resolve({ status: 200, message: "Images has been added." });
                });
        });
    }

    async deleteAllCarImages(carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`delete from car_images where car_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve();
                });
        });
    }

    //availability

    async addCarAvailability(carId, availablity) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into car_availability(
                availability_id, time_from, time_to
                , min_hours, max_hours, monday, tuesday, wednesday, thursday, friday, saturday, sunday, is_available,
                is_driver_available,fuel_included,is_captain_available)
             values ('${carId}','${availablity.time_from}','${availablity.time_to}','${availablity.min_hours}'
             ,'${availablity.max_hours}','${availablity.days[0] ? 1 : 0}','${availablity.days[1] ? 1 : 0}'
             ,'${availablity.days[2] ? 1 : 0}','${availablity.days[3] ? 1 : 0}','${availablity.days[4] ? 1 : 0}'
             ,'${availablity.days[5] ? 1 : 0}','${availablity.days[6] ? 1 : 0}','${1}','${availablity.is_driver_available ? 1 : 0}',
             '${availablity.is_fuel_included ? 1 : 0}','${availablity.is_captain_available ? 1 : 0})`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve();
            });
        });
    }

    async updateCarAvailability(carId, availablity) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car_availability set 
            time_from = '${availablity.time_from}', time_to = '${availablity.time_to}', min_hours ='${availablity.min_hours}', 
                max_hours = '${availablity.max_hours}', monday = '${availablity.days[0] ? 1 : 0}', tuesday = '${availablity.days[1] ? 1 : 0}',
                wednesday = '${availablity.days[2] ? 1 : 0}', thursday = '${availablity.days[3] ? 1 : 0}', friday = '${availablity.days[4] ? 1 : 0}', 
                saturday = '${availablity.days[5] ? 1 : 0}', sunday = '${availablity.days[6] ? 1 : 0}', is_available = '${availablity.is_available ? 1 : 0}'
                , is_driver_available = '${availablity.is_driver_available ? 1 : 0}', fuel_included = '${availablity.is_fuel_included ? 1 : 0}'
                where availability_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: "Availability has been updated." });
                });
        });
    }

    async setAvailabilityStatus(status, carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car_availability set 
                is_available = '${status ? 1 : 0}' where availability_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: "Availability has been updated." });
                });
        });
    }

    async setCarDriverAvailabilityStatus(status, carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car_availability set 
            is_driver_available = '${status ? 1 : 0}' where availability_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: "Availability has been updated." });
                });
        });
    }

    async setCarFuelAvailabilityStatus(status, carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car_availability set 
            fuel_included = '${status ? 1 : 0}' where availability_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: "Availability has been updated." });
                });
        });
    }

    async setCarCaptainAvailabilityStatus(status, carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update car_availability set 
            is_captain_available = '${status ? 1 : 0}' where availability_id = '${carId}'`,
                (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: "Availability has been updated." });
                });
        });
    }

    //features

    async addCarFeature(features) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into car_features_included(feature_id, car_id)
                 values ?`,
                [features.map(item => [item.feature_id, item.car_id])], (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else resolve();
                });
        });
    }

    async deleteAllCarFeatures(carId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`delete from car_features_included where car_id = '${carId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                else resolve();
            });
        });
    }

    async createFeature(name) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into feature(
                feature_name
            ) values('${name}')`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "Feature hase been created." });
            });
        });
    }

    async updateFeature(id, name) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update feature set feature_name = '${name}' where id = '${id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.affectedRows > 0) resolve({ status: 200, message: "Feature has been updated." });
                else resolve({ status: 404, message: "No record found with this id." });
            });
        });
    }


    async getAllFeatures() {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from feature`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No feature found." })
                else resolve({ status: 200, features: res });
            });
        });
    }
}

module.exports = CarModel