const dbConnection = require("../models/db_config");

class UserModel {

    // instance = new UserModel()
    // static getInstance(){
    //     return this.instance
    // }

    async createUser(phoneNumber) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into user(phone_number, verification_code) values('${phoneNumber}','${this.getOTPCode()}')`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve(res.insertId);
            });
        });
    }

    async isUserExist(phoneNumber) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user where phone_number = '${phoneNumber}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.length) resolve(res[0]);
                else resolve(false);
            });
        });
    }
    async getProfileStatus(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from profile_status where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.length) resolve({ status: 200, data: res });
                else reject({ status: 404, message: "no data available against this user." });
            });
        });
    }

    async getUserData(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No user found." });
                else resolve(res[0]);
            });
        });
    }
    async craeteProfileStatus(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert profile_status 
            (user_id, is_number_verified, is_password_settled, has_profile_created, cnic_uploaded, license_uploaded, document_verified) 
                values('${userId}','0','0','0','0','0','0')`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "User has been created.", data: { user_id: userId } });
            });
        });
    }

    async verifyPhoneVerificationCode(phoneNumber, code) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user where phone_number = '${phoneNumber}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) {
                    reject({ status: 404, message: "No User found with this phone number." });
                }
                else if (res[0].verification_code == code) {
                    resolve(res[0])
                }
                else reject({ status: 409, message: "Invalid code." });
            });
        });
    }

    async requestOTP(phoneNumber) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user set verification_code = '${this.getOTPCode()}' where phone_number = '${phoneNumber}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve(res);
            });
        });
    }

    async setPassword(id, password) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user set password = '${password}' where user_id = '${id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "Password has been settled." });
            });
        });
    }

    async getUserById(id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user where user_id = '${id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) {
                    reject({ status: 404, message: "No User found." });
                }
                else resolve(res);
            });
        });
    }
    async updatePassword(id, password) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user set password = '${password}' where user_id = '${id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "Password has been updated." });
            });
        });
    }

    async updateProfile(user) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user set name='${user.name}', email = '${user.email == null ? '' : user.email}', id_card_number = '${user.id_card_number}', city='${user.city}',
                country = '${user.country}', state='${user.state}', gender='${user.gender}', 
                account_type='${user.account_type}' where user_id = '${user.user_id}'`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else resolve({ status: 200, message: "Profile has been updated." });
                });
        });
    }

    async signIn(phone_number, password) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user where phone_number = '${phone_number}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (res.length) {
                    if (res[0].password == password) {
                        resolve(res[0])
                    }
                    else reject({ status: 409, message: "Invalid phone number or password." })
                }
                else reject({ status: 404, message: "No user found with this phone number." });
            });
        });
    }

    async uploadCnic(id, id_front, id_back) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`insert into user_document(user_id,cnic_1,cnic_2) 
            values('${id}','${id_front}','${id_back}','${license_front}','${license_back}')`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else resolve();
                });
        });
    }

    async updateCnic(id, id_front, id_back) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user_document set user_id='${id}',
            cnic_1='${id_front}',cnic_2='${id_back}'`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: 'CNIC has been updated.' });
                });
        });
    }

    async updateLicense(id, license_front, license_back) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user_document set user_id='${id}',car_license_1='${license_front}',
            car_license_2='${license_back}'`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (res.affectedRows == 0) reject({ status: 404, message: "No car found with this id." })
                    else resolve({ status: 200, message: 'Lisence has been updated.' });
                });
        });
    }

    async getDocuments(id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from user_document where user_id='${id}'`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (!res) reject({ status: 404, message: "No record found." });
                    else resolve({ status: 200, documents: res });
                });
        });
    }

    async getVerifiedAndUnverifiedDocuments(verified) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select
            Car_rental.user.user_id,Car_rental.user.name as user_name,Car_rental.user.account_type as user_account_type,
            Car_rental.user_document.id as document_id,Car_rental.user_document.cnic_1,Car_rental.user_document.cnic_2,
            Car_rental.user_document.car_license_1,Car_rental.user_document.car_license_2,Car_rental.user_document.denial_reason
            from Car_rental.user
            join Car_rental.profile_status on Car_rental.profile_status.user_id = Car_rental.user.user_id 
            and Car_rental.profile_status.documents_uploaded = '1' and Car_rental.profile_status.document_verified = '${verified}' 
            join Car_rental.user_document on Car_rental.user_document.user_id = Car_rental.user.user_id`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (!res.length) reject({ status: 404, message: "No record Found" });
                    else resolve({ status: 200, documents: res });
                });
        });
    }

    async getDocumentsOfAllUsers() {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(` select
            Car_rental.user.user_id,Car_rental.user.name as user_name,Car_rental.user.account_type as user_account_type,
            Car_rental.user_document.id as document_id,Car_rental.user_document.cnic_1,Car_rental.user_document.cnic_2,
            Car_rental.user_document.car_license_1,Car_rental.user_document.car_license_2,Car_rental.user_document.denial_reason
            from Car_rental.user
            join Car_rental.profile_status on Car_rental.profile_status.user_id = Car_rental.user.user_id 
            and Car_rental.profile_status.documents_uploaded = '1'
            join Car_rental.user_document on Car_rental.user_document.user_id = Car_rental.user.user_id`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (!res.length) reject({ status: 404, message: "No record Found" });
                    else resolve({ status: 200, documents: res });
                });
        });
    }

    async writeReviewOnDocument(documentId, review) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update user_document set denial_reason='${review}' where id = '${documentId}'`
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else resolve({ status: 200, message: "review has been written against documents." });
                });
        });
    }

    async getAllUsers(account_type) {
        let query = ""
        if (account_type == "admin") query = `select * from user where account_type = '${account_type}'`
        else query = `select * from user where account_type in('${account_type}','both')`
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(query
                , (err, res) => {
                    if (err) reject({ status: 500, message: err.message });
                    else if (!res.length) reject({ status: 404, message: "No User Found." });
                    else resolve({ status: 200, users: res });
                });
        });
    }

    //profile status update code
    async updateUserPhoneVerification(user_id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set is_number_verified = '1' where user_id = '${user_id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "phone number has been verified." });
            });
        });
    }

    async updateUserSetPassword(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set is_password_settled = '1' where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "phone number has been verified." });
            });
        });
    }

    async updateUserProfileCreated(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set has_profile_created = '1' where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "phone number has been verified." });
            });
        });
    }

    async updateUserCnicUploaded(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set cnic_uploaded = '1' where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "documens has been uploaded." });
            });
        });
    }

    async updateUserLicenseUploaded(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set license_uploaded = '1' where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "documens has been uploaded." });
            });
        });
    }

    async updateUserDocumentVerification(userId) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update profile_status set document_verified = '1' where user_id = '${userId}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else resolve({ status: 200, message: "documents have been verified." });
            });
        });
    }

    getOTPCode() {
        return Math.floor(100000 + Math.random() * 900000);
    }

}

module.exports = UserModel