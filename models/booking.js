const dbConnection = require("../models/db_config");

class BookingModel {


    async createBooings(booking, user_id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`
            insert into booking() valuse()`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else {
                    res.map(item => { item.images = item.images.split(",") })
                    resolve({ status: 200, cars: res });
                }
            });
        });
    }

    async getAllCustomerBooking(user_id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from booking where customer_id = '${user_id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No booking found." })
                else resolve({ status: 200, cars: res });
            });
        });
    }

    async getVendorBookings(user_id) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`select * from booking where vendor_id = '${user_id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No booking found." })
                else resolve({ status: 200, cars: res });
            });
        });
    }

    async updateBookingStatus(booking_id, status) {
        return new Promise((resolve, reject) => {
            dbConnection.connection.query(`update booking set booking_status = '${status}' where booking_id = '${booking_id}'`, (err, res) => {
                if (err) reject({ status: 500, message: err.message });
                else if (!res.length) reject({ status: 404, message: "No booking found." })
                else resolve({ status: 200, cars: res });
            });
        });
    }
}


module.exports = BookingModel